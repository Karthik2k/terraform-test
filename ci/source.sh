function GetArmKeys {
  eval $(echo ${ARM_CICD_KEYS} | base64 -d)
}

function RenderTfLoggingProvider {
  cat <<- EOF > logging-provider.tf
	provider "azurerm" {
	  alias = "logging"
	  subscription_id = "${1}"
	  features {}
	}
	EOF
}

function RunTfSec {
  cp /build/tfsec-linux-amd64 .
  ./tfsec-linux-amd64
  for dir in */ ; do
      echo "Processing $dir" && ./tfsec-linux-amd64 $dir
  done
}

function RunTfFmt {
  terraform fmt
  for dir in */ ; do
    echo "Processing $dir" && terraform fmt -check $dir
  done 
}

function RunTfLint {
  cp /build/.tflint.hcl .
  cp /build/tflint .
  cp /build/Makefile .
  make tf-lint
}

function RunTfConventionChecker {
  cp /build/HCLPolicyChecker .
  cp /build/hcl_policy_checker.yml .
  cp /build/Makefile .
  make tf-convention-check
}

function RunTerratest {
  GetArmKeys
  go mod init tests
  cd tests
  echo " timeout value : $TERRATEST_TIMEOUT_VALUE"
  go test -v -timeout $TERRATEST_TIMEOUT_VALUE
}

function RunTerraformInit {
  GetArmKeys
  RenderTfLoggingProvider ${TF_PROVIDER_LOGGING_SUBSCRIPTION_ID}
  gitlab-terraform -v
  gitlab-terraform init
}

function RunTerraformValidate {
  GetArmKeys
  RenderTfLoggingProvider ${TF_PROVIDER_LOGGING_SUBSCRIPTION_ID}
  gitlab-terraform validate
}

function RunTerraformPlan {
  GetArmKeys
  RenderTfLoggingProvider ${TF_PROVIDER_LOGGING_SUBSCRIPTION_ID}
  gitlab-terraform init -lock=false
  gitlab-terraform refresh -lock=false
  gitlab-terraform plan
  gitlab-terraform show -json plan.cache > plan.json
}

function RunTerraformApply {
  GetArmKeys
  RenderTfLoggingProvider ${TF_PROVIDER_LOGGING_SUBSCRIPTION_ID}
  gitlab-terraform apply
}

function RunValidateTerraformPlan {
  GetArmKeys
  RenderTfLoggingProvider ${TF_PROVIDER_LOGGING_SUBSCRIPTION_ID}
  python3 --version
  cat plan.json | python3 /validate_plan/check_destructive_changes.py
  eval "$(cat changes.txt)"
  if [[ "true" == "${DESTRUCTIVE_CHANGES}"  ]]; then exit 1; fi
}

function RunValidatePlanChanges {
  GetArmKeys
  RenderTfLoggingProvider ${TF_PROVIDER_LOGGING_SUBSCRIPTION_ID}
  python3 --version
  python3 /validate_plan/validate_plan.py
}

function RunTerraformDestroy {
  GetArmKeys
  RenderTfLoggingProvider ${TF_PROVIDER_LOGGING_SUBSCRIPTION_ID}
  gitlab-terraform destroy
}

function RunAnsiblePlaybook () {
  IFS=%
  # Process named arguments
  while [ $# -gt 0 ]; do
    if [[ $1 == *"--"* ]]; then
      v="${1/--/}"
      declare $v="$2"
    fi
    shift
  done

  declare -a command
  command+="ansible-playbook"
  # command+=" -v"
  [ -n "$inventoryFile" ] && command+=" --inventory $inventoryFile"
  [ -n "$extraVarsFile" ] && command+=" --extra-vars @$extraVarsFile"
  [ -n "$sshExtraArgs" ] && command+=" --ssh-extra-args=\"$sshExtraArgs\""
  command+=" $playbookPath"

  echo "Arguments for ansible-playbook are:"
  echo "$command"
  ls . inventory ssh vars
  bash -c "$command"
  unset IFS
}
