#!/bin/sh

cat <<'EOF'
stages:
  - Prepare Pipeline
  - Prepare Repos
  - Setup
  - Test
  - Reset
  - Static Analysis
  - Integration Testing
  - Terraform Validate
  - Terraform Plan
  - Validate Plan
  - Build
  - Pages Build
  - Clone Repo
  - Copy to AS
  - Deploy
  - Configure
  - Destroy

variables:
  TERRAFORM_ENABLED: 'true'
  TERRAFORM_DYNAMIC_PIPELINE: 'true'
  BRANCHING_MODEL: 'tbd'
  ESLINT_ENABLED: 'false'
  DOCKER_BUILD_REQUIRED: 'false'
  TERRAFORM_DEPLOY_FLOW_ENABLED: 'true'
  ANSIBLE_PLAYBOOK_ENABLED: 'true'

workflow:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "parent_pipeline"'

.terraform_before_script:
  image: registry.gitlab.com/gitlab-org/terraform-images/releases/0.14:v0.10.0
  variables:
    LOCAL_SEF_CI_LIBRARY_SCRIPTS_PATH: ./cicd-catalog/scripts
  before_script:
    - source ./ci/source.sh
    - git config --global url."https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com".insteadOf https://gitlab.com
    - terraform version

.ansibleplaybook_before_script:
  variables:
    ANSIBLE_FORCE_COLOR: 'true'
  image: registry.gitlab.com/atosbps/sef-core/devops-tooling/cicd-containers/sef-cicd-container-registry/molecule-container:latest
  before_script:
    - source ./ci/source.sh

.ansibleplaybook_deployment_rules:
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $TERRAFORM_DEPLOY_FLOW_ENABLED == "true" && $ANSIBLE_PLAYBOOK_ENABLED == "true"'

.terraform_deployment_rules:
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $TERRAFORM_DEPLOY_FLOW_ENABLED == "true"'
EOF

for env in $(find environments -type f -name "*.json"); do
  NAME=$(basename ${env} | cut -d\. -f1)
  TARGET_DIR="$(dirname ${env})"
  TERRAFORM_DIR="./blueprints/sapiens_wrapper"
  ANSIBLE_DIR="./ansible"
  ENVIRONMENT="$(basename ${TARGET_DIR})"
  TENANT_NAME="$(basename $(dirname ${TARGET_DIR}) | /usr/bin/sed -e 's/_/-/g')"
  SUBSCRIPTION_NAME="$(echo ${ENVIRONMENT} | tr -s '[:lower:]' '[:upper:]' | /usr/bin/sed -e 's/-/_/g')"
  case ${TENANT_NAME} in
    "atosbps-sef-nonprod")
      ARM_CICD_KEYS="\${ARM_ATOSBPS_SEF_NONPROD_BPS_SEF_SANDBOX}"
    ;;
    "atosbps-sef-devsecops")
      ARM_CICD_KEYS="\${ARM_ATOSBPS_SEF_DEVSECOPS_${SUBSCRIPTION_NAME}}"
      TF_PROVIDER_LOGGING_SUBSCRIPTION_ID="f43a9639-b6f8-4f55-ba01-d8b688a9c3e7"
    ;;
    "aegon-bps-logging")
      ARM_CICD_KEYS="\${ARM_AEGON_BPS_LOGGING_ef3ec399a227}"
    ;;
    "aegon-eb-devops")
      ARM_CICD_KEYS="\${ARM_AEGON_EB_DEVOPS_c903b8a7c5e8}"
    ;;
    *)
      echo "Error: Unknown TENANT_NAME. Credentials are not mapped."
      exit 1
    ;;
  esac
  cat <<EOF

.anchor-${NAME}:
  variables:
    ARM_CICD_KEYS: ${ARM_CICD_KEYS}
    ANSIBLE_PLAYBOOK_PATH: "deploy.yml"
    ANSIBLE_INVENTORY_PATH: "inventory/tf_ansible_inventory_${NAME}"
    ANSIBLE_EXTRA_VARS: "vars/tf_ansible_vars_file_${NAME}.yml"
    ANSIBLE_SSH_EXTRA_ARGS: "-F ssh/${NAME}-ssh-config"
    TF_PROVIDER_LOGGING_SUBSCRIPTION_ID: "${TF_PROVIDER_LOGGING_SUBSCRIPTION_ID}"
    TF_VAR_json_path: \${CI_PROJECT_DIR}/${env}
    TF_CLI_ARGS_init: |
      -backend-config="storage_account_name=seftfstatemgmt"
      -backend-config="container_name=sef-tfstate-mgmt"
      -backend-config="key=\${CI_PROJECT_NAME}/master/${TENANT_NAME}/${ENVIRONMENT}/${NAME}/terraform.tfstate"

.terraform-destroy-rules-${NAME}:
  rules:
    - if: '\$CI_PIPELINE_SOURCE == "schedule" && \$TERRAFORM_SCHEDULE_ACTION == "STOP" && \$ENVIRONMENT_NAME == "${NAME}"'

validate-${NAME}:
  extends:
    - .terraform_before_script
    - .anchor-${NAME}
  stage: Terraform Validate
  script:
    - cd ${TERRAFORM_DIR}
    - RunTerraformValidate

plan-${NAME}:
  extends:
    - .terraform_before_script
    - .anchor-${NAME}
  stage: Terraform Plan 
  script:
    - cd ${TERRAFORM_DIR}
    - RunTerraformPlan
  artifacts:
    paths:
      - ${TERRAFORM_DIR}/plan.json
      - ${TERRAFORM_DIR}/plan.cache

deploy-${NAME}:
  extends:
    - .terraform_before_script
    - .terraform_deployment_rules
    - .anchor-${NAME}
  stage: Deploy
  script:
    - cd ${TERRAFORM_DIR}
    - RunTerraformApply
  artifacts:
    name: ansible-outputs-${NAME}
    paths:
      - ansible/ssh
      - ansible/inventory
      - ansible/vars
  dependencies:
    - plan-${NAME}

# ansibleplaybook-${NAME}:
#   extends:
#     - .ansibleplaybook_before_script
#     - .ansibleplaybook_deployment_rules
#     - .anchor-${NAME}
#   stage: Configure
#   script:
#     - cd ${ANSIBLE_DIR}
#     - |
#       RunAnsiblePlaybook \
#         --playbookPath \$ANSIBLE_PLAYBOOK_PATH \
#         --inventoryFile \$ANSIBLE_INVENTORY_PATH \
#         --extraVarsFile \$ANSIBLE_EXTRA_VARS \
#         --sshExtraArgs "\$ANSIBLE_SSH_EXTRA_ARGS"
#   dependencies:
#     - deploy-${NAME}

destroy-${NAME}:
  extends:
    - .terraform_before_script
    - .terraform-destroy-rules-${NAME}
    - .anchor-${NAME}
  stage: Destroy
  script:
    - cd ${TERRAFORM_DIR}
    - RunTerraformDestroy

EOF
done
