#!/bin/bash
cat <<'EOF'
stages:
  - Prepare Pipeline
  - Prepare Repos
  - Setup
  - Test
  - Reset  
  - Static Analysis
  - Integration Testing
  - Terraform Validate
  - Terraform Plan
  - Validate Plan
  - Build
  - Test
  - Pages Build
  - Clone Repo
  - Copy to AS
  - Deploy
  - Configure
  - Destroy

variables:
  TERRAFORM_ENABLED: 'true'
  TERRAFORM_DYNAMIC_PIPELINE: 'true'
  BRANCHING_MODEL: 'tbd'
  ESLINT_ENABLED: 'false'
  DOCKER_BUILD_REQUIRED: 'false'
  TERRAFORM_DEPLOY_FLOW_ENABLED: 'true'
  ANSIBLE_PLAYBOOK_ENABLED: 'true'

.default_before_script:
  variables:
      REMOTE_CI_LIBRARY_SCRIPTS_PATH: ./cicd-catalog/scripts
  before_script:
    - if [ "$CI_MERGE_REQUEST_TARGET_BRANCH_NAME" == "master" ] && [ "$CI_PIPELINE_SOURCE" == "merge_request_event" ] && [ "$TEST_CONTAINER_REGISTRY_ENABLED" == "true" ]; then export CONTAINER_REGISTRY_PATH=$TEST_CONTAINER_REGISTRY_PATH && export CI_CONTAINER_REGISTRY_USER=$CI_TEST_CONTAINER_REGISTRY_USER && export CI_CONTAINER_REGISTRY_PASSWORD=$CI_TEST_CONTAINER_REGISTRY_PASSWORD; fi
    - if [ "$CI_MERGE_REQUEST_TARGET_BRANCH_NAME" == "develop" ] && [ "$CI_PIPELINE_SOURCE" == "merge_request_event" ] && [ "$TEST_CONTAINER_REGISTRY_ENABLED" == "true" ]; then export CONTAINER_REGISTRY_PATH=$TEST_CONTAINER_REGISTRY_PATH && export CI_CONTAINER_REGISTRY_USER=$CI_TEST_CONTAINER_REGISTRY_USER && export CI_CONTAINER_REGISTRY_PASSWORD=$CI_TEST_CONTAINER_REGISTRY_PASSWORD; fi
    - if [ "$CI_PIPELINE_SOURCE" == "push" ] && [ "$CI_COMMIT_BRANCH" == "develop" ] && [ "$TEST_CONTAINER_REGISTRY_ENABLED" == "true" ]; then export CONTAINER_REGISTRY_PATH=$TEST_CONTAINER_REGISTRY_PATH && export CI_CONTAINER_REGISTRY_USER=$CI_TEST_CONTAINER_REGISTRY_USER && export CI_CONTAINER_REGISTRY_PASSWORD=$CI_TEST_CONTAINER_REGISTRY_PASSWORD; fi
    - echo "$CONTAINER_REGISTRY_PATH"

step-1:
  extends:
    - .default_before_script
  stage: Test
  script:
    - echo "Can be canceled."
EOF
cat <<'EOF'

step-2:
  extends:
    - .default_before_script
  stage: Test
  script:
    - echo "Can be canceled."
EOF
