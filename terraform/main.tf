terraform {
  backend "azurerm" {}
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.46.0"
    }
    azuread = {
      source  = "hashicorp/azuread"
      version = "1.4.0"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "~> 3.1"
    }
  }
}

variable "gitlab_access_token" {
  type = string
}

provider "gitlab" {
  token = var.gitlab_access_token
}

data "gitlab_project" "terraform-test" {
  id = 28449250
}
